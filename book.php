
<?php

session_start();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="description" content="Decision Tourism Support System is a tourism system that will help tourists view places of Uganda that are worth to visit">

    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Decision Tourism Support System">
    <meta property="og:title" content="Dashboard | Decision Tourism Support System">

    <title>Dashboard | Decision Tourism Support System </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" href="css/font-awesome.min.css">
</head>
<body class="app sidebar-mini rtl">
<!-- Navbar-->
<header class="app-header"><a class="app-header__logo" href="#">TDS System</a>
    <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
        <li class="app-search">
            <input class="app-search__input" type="search" placeholder="Search">
            <button class="app-search__button"><i class="fa fa-search"></i></button>
        </li>
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"> <b><?php echo $_SESSION['username']; ?></b>  </i></a>
            <ul class="dropdown-menu settings-menu dropdown-menu-right">
                <li><a class="dropdown-item" href="#"><i class="fa fa-user fa-lg"></i> Profile</a></li>
                <li><a class="dropdown-item" href="index.php"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
            </ul>
        </li>
    </ul>
</header>
<!-- Sidebar menu-->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <ul class="app-menu">
        <li><a class="app-menu__item active" href="dashboard.php"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>
        <li><a class="app-menu__item" href="tours.php"><i class="app-menu__icon fa fa-pie-chart"></i><span class="app-menu__label">Latest Tours</span></a></li>
        <li><a class="app-menu__item" href="game.php"><i class="app-menu__icon fa fa-pie-chart"></i><span class="app-menu__label">National Parks</span></a></li>
        <li><a class="app-menu__item" href="interesting.php"><i class="app-menu__icon fa fa-pie-chart"></i><span class="app-menu__label">Interesting Sites</span></a></li>
    </ul>
</aside>
<main class="app-content">
    <div class="app-title">
        <div>
            <h1>Booking</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="#">Booking</a></li>
        </ul>
    </div>



    <div class="tile mb-4">
        <div class="row">
            <div class="col-lg-12">
                <div class="sects">
                   <p>

                       <?php
                       /* Attempt MySQL server connection. */
                       $link = mysqli_connect(
                           "localhost",
                           "root",
                           "",
                           "tourism");

                       // Check connection
                       if($link === false){
                           die("ERROR: Could not connect. " . mysqli_connect_error());
                       }

                       // Escape user inputs for security
                       $fullname = mysqli_real_escape_string($link, $_REQUEST['fullname']);
                       $phone = mysqli_real_escape_string($link, $_REQUEST['phone']);
                       $email = mysqli_real_escape_string($link, $_REQUEST['email']);
                       $gender = mysqli_real_escape_string($link, $_REQUEST['gender']);
                       $country = mysqli_real_escape_string($link, $_REQUEST['country']);
                       $site = mysqli_real_escape_string($link, $_REQUEST['site']);
                       $arrivalDate = mysqli_real_escape_string($link, $_REQUEST['arrivalDate']);
                       $departDate = mysqli_real_escape_string($link, $_REQUEST['departDate']);
                       $comment = mysqli_real_escape_string($link, $_REQUEST['comment']);

                       // Attempt insert query execution
                       $sql = "INSERT INTO booking (fullname, phone, email, gender, country, site, arrivalDate, departDate, comment)
            VALUES ('$fullname', '$phone', '$email', '$gender', '$country', '$site', '$arrivalDate', '$departDate', '$comment')";
                       if(mysqli_query($link, $sql)){
                           echo "we have successfully received your request, we shall be notifying you via telephone about your tour";
                       } else{
                           echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
                       }

                       // Close connection
                       mysqli_close($link);
                       ?>

                   </p>
                </div>
            </div>

        </div>
    </div>







</main>
<!-- Essential javascripts for application to work-->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
<!-- The javascript plugin to display page loading on top-->
<script src="js/plugins/pace.min.js"></script>
<!-- Page specific javascripts-->
<script type="text/javascript" src="js/plugins/chart.js"></script>

</body>
</html>
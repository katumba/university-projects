
<?php

session_start();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="description" content="Decision Tourism Support System is a tourism system that will help tourists view places of Uganda that are worth to visit">

    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Decision Tourism Support System">
    <meta property="og:title" content="Dashboard | Decision Tourism Support System">

    <title>Dashboard | Decision Tourism Support System </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" href="css/font-awesome.min.css">
</head>
<body class="app sidebar-mini rtl">
<!-- Navbar-->
<header class="app-header"><a class="app-header__logo" href="#">TDS System</a>
    <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
        <li class="app-search">
            <input class="app-search__input" type="search" placeholder="Search">
            <button class="app-search__button"><i class="fa fa-search"></i></button>
        </li>
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg">  <b><?php echo $_SESSION['username']; ?></b>   </i></a>
            <ul class="dropdown-menu settings-menu dropdown-menu-right">
                <li><a class="dropdown-item" href="#"><i class="fa fa-user fa-lg"></i> Profile</a></li>
                <li><a class="dropdown-item" href="index.php"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
            </ul>
        </li>
    </ul>
</header>
<!-- Sidebar menu-->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <ul class="app-menu">
        <li><a class="app-menu__item" href="dashboard.php"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>
        <li><a class="app-menu__item" href="tours.php"><i class="app-menu__icon fa fa-pie-chart"></i><span class="app-menu__label">Latest Tours</span></a></li>
        <li><a class="app-menu__item active" href="game.php"><i class="app-menu__icon fa fa-pie-chart"></i><span class="app-menu__label">National Parks</span></a></li>
        <li><a class="app-menu__item" href="interesting.php"><i class="app-menu__icon fa fa-pie-chart"></i><span class="app-menu__label">Interesting Sites</span></a></li>
    </ul>
</aside>
<main class="app-content">
    <div class="app-title">
        <div>
            <h1>National parks</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="#">National parks</a></li>
        </ul>
    </div>



    <div class="tile mb-4">
        <div class="row">
            <div class="col-lg-6">
                <h4>Bwindi Impenetrable National Park </h4>
                <div class="sects">
                    <div class="tile-body"> <img src="images/bwindi.JPG"  style="width: 500px; height: 390px"  alt=""></div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="bs-component">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home">Brief about</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#profile">Accommodation</a></li>
                    </ul>
                    <hr>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active show zilk" id="home">
                            <p>
                                <b>Bwindi Impenetrable National Park</b> found in south-western Uganda is a home to the endangered mountain gorillas in Uganda.
                                <b>Bwindi Forest</b> is famous for Gorilla Trekking Safaris in Uganda and is situated along the Democratic Republic of Congo
                                border next to the Virunga National Park. Bwindi is on the edge of the western Great Rift Valley. It comprises 331 square
                                kilometres of jungle forests which comprises of both montane and lowland forest and is accessible only on foot.
                            </p>
                            <p>
                                Bwindi National Park is one of the 3 <b>UNESCO World Heritage Sites in Uganda</b>. The park has one
                                of the richest ecosystems in Africa and a variety of animal and bird species. The park is a
                                habitat for some 120 species of mammals, 346 species of birds, 202 species of butterflies, 163
                                species of trees, 100 species of ferns, 27 species of frogs, chameleons, geckos and
                                many endangered species. In particular the area shares in the high levels of endemic of the Albertine Rift.
                            </p>
                        </div>
                        <div class="tab-pane fade zilk" id="profile">
                            <p>
                                Bwindi is blessed with a number of affordable accommodation facilities of almost all class of people.
                                Accommodation below is of luxurious and relative in setting:-
                            </p>

                            <ol>
                                <li>Gorilla Safari Lodge</li>
                                <li>Chameleon Hill Lodge</li>
                                <li>Clouds Mountain Gorilla Lodge</li>
                                <li>Buhoma Community Rest Camp</li>
                                <li>Kitandara Lodge</li>
                                <li>Silverback Lodge</li>
                                <li>Mahogany Springs</li>
                            </ol>

                        </div>
                    </div>
                </div>
                <hr>
                <div class="tour">
                    <a href="booking.php"> <button type="button" class="btn btn-success">Book and tour the place</button> </a>
                </div>
            </div>
        </div>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-lg-6">
                <h4>Mgahinga National Park</h4>
                <div class="sects">
                    <img src="images/mghagiga.JPG" style="width: 500px; height: 390px" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="bs-component">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home1">Brief about</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#accomodation1">Accommodation</a></li>
                    </ul>
                    <hr>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active show zilk" id="home1">
                            <p>
                                <b>Mgahinga Gorilla National Park</b> is found in far southwestern Uganda, bordering Rwanda
                                in the south and Democratic Republic of Congo (DRC) in the west, with a geographical
                                coverage of 33 square kilometres. Located 14 kms from Kisoro town, the park covers the northern slopes of the three
                                Virunga Volcanoes including Mountain Muhavura (4,127 m), Mountain Gahinga (3,474 m) and Mountain Sabinyo (3,645 m).
                            </p>
                            <p>
                                Mgahinga Gorilla National Park consists of the partly forested slopes of three extinct volcanoes.
                                From far away, the huge cones of the Virunga Volcanoes dominate the landscape and beckon you as
                                you approach. When you reach the park you can get a great scenery of the area by walking up to the
                                viewpoint located 15 minutes from Ntebeko Gate. The park has great biological importance since throughout
                                the climatic changes of the Pleistocene ice ages, mountains like this one provided a refuge for mountain plants
                                and animals that moved up or down the slopes as climate became warmer or cooler.
                                The Virungas are home to a large variety of wildlife including the world’s seriously endangered mountain gorillas.
                            </p>
                        </div>
                        <div class="tab-pane fade zilk" id="accomodation1">
                            <p>If you are the ‘go camping tourist’, then there is ample space at the park gate and Mt.
                                Gahinga Rest Camp for ‘Do it Yourself Camping’, besides that, there is also excellent traditional
                                Bandas managed by the local community. Kisoro Town is just 14 kms to the gate of Mgahinga Gorilla National
                                Park and offers a variety
                                of accommodation facilities like the basic Camp-site, luxurious full board hotels and these include:</p>

                            <ol>
                                <li>Traveller's Rest Hotel Kisoro</li>
                                <li>Sky Blue Hotel</li>
                                <li>Mount Gahinga Rest Camp</li>
                                <li>Virunga Hotel</li>
                                <li>Mgahinga Safari Lodge</li>
                                <li>Rugigana Campsite</li>
                                <li>Mubano Hotel</li>
                            </ol>

                        </div>
                    </div>
                </div>
                <hr>
                <div class="tour">
                    <a href="booking.php"> <button type="button" class="btn btn-success">Book and tour the place</button> </a>
                </div>
            </div>
        </div>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-lg-6">
                <h4>Murchison Falls National Park</h4>
                <div class="sects">
                    <img src="images/murchison.JPG" style="width: 500px; height: 390px" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="bs-component">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home2">Brief about</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#accomodation2">Accommodation</a></li>
                    </ul>
                    <hr>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active show zilk" id="home2">
                            <p>
                                <b>Murchison Falls National Park</b> is one of the biggest national parks in Uganda with lots of
                                wildlife safaris and tours taking place frequently. The park under the Murchison Falls
                                Conservation Area is the largest protected area in Uganda covering about 3,893 km comprising
                                of Murchison Falls National Park, Bugungu Wildlife Reserve and Karuma Falls Wildlife Reserves
                                and in the Southern section, Budongo Forest. The park is also characterized by beautiful and
                                magnificent Top of the Falls, excellent accommodation facilities or lodges, wild animals and
                                birds plus striking plains that offer nice views during game drive safaris.
                                The park’s attractions are highly enjoyed by visitors who do a 3 days Uganda safari or maximum for days.
                            </p>
                        </div>
                        <div class="tab-pane fade zilk" id="accomodation2">
                            <p>
                                <b>Murchison Falls National Park</b> has got several accommodation
                                facilities ranging from luxury , midrange to budget lodges and
                                campsites. The park also has traditional and self-contained bandas
                                as well. Prominent at the park is the luxurious Paraa Safari Lodge
                                which offers the best services to visitors at the park.
                                Accommodation centres include:
                            </p>

                            <ol>
                                <li>Paraa Safari Lodge</li>
                                <li>Nile Safari Camp</li>
                                <li>Sambiya River Lodge</li>
                                <li>Murchison River Lodge</li>
                                <li>Rabongo Eco-Tourism Centre (good cottages)</li>
                                <li>Pakuba Safari Lodge</li>
                                <li>Red Chili Rest Camp</li>

                            </ol>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="tour">
                    <a href="booking.php"> <button type="button" class="btn btn-success">Book and tour the place</button> </a>
                </div>
            </div>
        </div>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-lg-6">
                <h4>Rwenzori Mountains National Park </h4>
                <div class="sects">
                    <img src="images/rwezori.JPG" style="width: 500px; height: 390px" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="bs-component">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home3">Brief about</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#accomodation3">Accommodation</a></li>
                    </ul>
                    <hr>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active show zilk" id="home3">
                            <p>
                                Standing as Uganda’s top trekking destination, <b>Rwenzori Mountains National Park</b>
                                has the highest mountain range in the whole of Africa, with 6 glacial peaks including
                                Mount Speke, Mount Stanley, Mount Gessi, Mount Emin Pasha, Mount Luigi da Savoia and Mount Baker.
                                The main peaks can be perhaps compared to those of Kilimanjaro and Mount Kenya. These two peaks,
                                Margherita (5.109m) and Alexandra (5.083m)
                                on Mount Stanley are adventurous for any adventurous mountain hiker in Africa.
                            </p>
                            <p>
                                Rwenzori is located in the western part of Uganda on the border of Uganda and the Democratic Republic
                                of Congo cover 998 sq km and was named after the famous Rwenzori Mountains which is also referred
                                to as the legendary ‘Mountains of the Moon’. The mountain has distinctive glacial peaks visible
                                miles away as one travels in the Western part of Uganda despite its existence almost at the Equator.
                                The Rwenzori Mountains offer spectacular scenery to the national park
                            </p>
                        </div>
                        <div class="tab-pane fade zilk" id="accomodation3">
                            <p>
                                Kasese town, a close neighbour to Rwenzori has many affordable accommodation facilities of all
                                classes of people. Rwenzori Mountaineering
                                Service also offers accommodation facilities like huts with bunk beds along the central circuit.

                            </p>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="tour">
                    <a href="booking.php"> <button type="button" class="btn btn-success">Book and tour the place</button> </a>
                </div>
            </div>
        </div>
    </div>

    <div class="tile mb-4">
        <div class="row">
            <div class="col-lg-6">
                <h4>Queen Elizabeth National Park Uganda</h4>
                <div class="sects">
                    <img src="images/queen.JPG" style="width: 500px; height: 390px" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="bs-component">
                    <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#home4">Brief about</a></li>
                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#accomodation4">Accommodation</a></li>
                    </ul>
                    <hr>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade active show zilk" id="home4">
                            <p>
                                <b>Queen Elizabeth National Park</b> is Uganda’s most popular game reserve for anyone
                                with a keen interest in safaris, beautiful nature and scenic view. The park established in
                                1954 was named after Queen Elizabeth II. Geographically, the park runs from the foothills of
                                the Rwenzori crater in the north to the shores of Lake Edward to the remote Ishasha River in the
                                south, Lake George and Kazinga Channel,
                                combining a wide variety of habitats like savanna, wetlands and lowland forests among others.
                            </p>
                        </div>
                        <div class="tab-pane fade zilk" id="accomodation4">
                            <p>
                                <b>Queen Elizabeth National Park</b> has got several accommodation
                                facilities ranging from luxury , midrange to budget lodges and
                                campsites. The park also has traditional and self-contained bandas
                                as well. Accommodation centres include:
                            </p>

                            <ol>
                                <li>Mweya Safari Lodge</li>
                                <li>Simba Safari Camp</li>
                                <li>Jacana Safari Lodge</li>
                                <li>The Institute of Ecology</li>
                                <li>Kyambura Safari Lodge</li>
                                <li>Ihamba Safari Lodge</li>
                                <li>Katara Lodge</li>
                            </ol>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="tour">
                    <a href="booking.php"> <button type="button" class="btn btn-success">Book and tour the place</button> </a>
                </div>
            </div>
        </div>
    </div>






</main>
<!-- Essential javascripts for application to work-->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
<!-- The javascript plugin to display page loading on top-->
<script src="js/plugins/pace.min.js"></script>
<!-- Page specific javascripts-->
<script type="text/javascript" src="js/plugins/chart.js"></script>

</body>
</html>
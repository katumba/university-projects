
<?php

session_start();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="description" content="Decision Tourism Support System is a tourism system that will help tourists view places of Uganda that are worth to visit">

    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Decision Tourism Support System">
    <meta property="og:title" content="Dashboard | Decision Tourism Support System">

    <title>Dashboard | Decision Tourism Support System </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" href="css/font-awesome.min.css">
</head>
<body class="app sidebar-mini rtl">
<!-- Navbar-->
<header class="app-header"><a class="app-header__logo" href="#">TDS System</a>
    <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
        <li class="app-search">
            <input class="app-search__input" type="search" placeholder="Search">
            <button class="app-search__button"><i class="fa fa-search"></i></button>
        </li>
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"> <b><?php echo $_SESSION['username']; ?></b>  </i></a>
            <ul class="dropdown-menu settings-menu dropdown-menu-right">
                <li><a class="dropdown-item" href="#"><i class="fa fa-user fa-lg"></i> Profile</a></li>
                <li><a class="dropdown-item" href="index.php"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
            </ul>
        </li>
    </ul>
</header>
<!-- Sidebar menu-->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <ul class="app-menu">
        <li><a class="app-menu__item " href="dashboard.php"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>
        <li><a class="app-menu__item" href="tours.php"><i class="app-menu__icon fa fa-pie-chart"></i><span class="app-menu__label">Latest Tours</span></a></li>
        <li><a class="app-menu__item" href="game.php"><i class="app-menu__icon fa fa-pie-chart"></i><span class="app-menu__label">National Parks</span></a></li>
        <li><a class="app-menu__item active" href="interesting.php"><i class="app-menu__icon fa fa-pie-chart"></i><span class="app-menu__label">Interesting Sites</span></a></li>
    </ul>
</aside>
<main class="app-content">
    <div class="app-title">
        <div>
            <h1>Interesting Sites</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="#">Interesting sites</a></li>
        </ul>
    </div>


    <div class="tile mb-4">
        <div class="row">
            <div class="col-lg-12">
                <div class="sects">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="tile">
                                <h3 class="tile-title">Mabira Foreset Reserve</h3>
                                <div class="tile-body tilk">
                                    <img src="images/mabira.JPG" style="width: 450px; height: 200px" alt="">
                                    <p>
                                        The Mabira Forest is a rainforest area covering about 300 square kilometres in
                                        Uganda, located in Buikwe District, between Lugazi and Jinja. It has been protected
                                        as Mabira Forest Reserve since 1932.
                                        It is home for many endangered species like the primate Lophocebus Uganda

                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="tile">
                                <h3 class="tile-title">Lake Victoria</h3>
                                <div class="tile-body tilk">
                                    <img src="images/victoria.JPG" style="width: 450px; height: 200px" alt="">
                                    <p>
                                        Lake Victoria is one of the African Great Lakes. The lake was renamed Lake Victoria
                                        after Queen Victoria by the explorer John Hanning Speke, in his reports—the first Briton to
                                        document it. Speke accomplished this in 1858, while on an
                                        expedition with Richard Francis Burton to locate the source of the Nile River.

                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="tile">
                                <h3 class="tile-title">River Nile</h3>
                                <div class="tile-body tilk">
                                    <img src="images/nile.JPG" style="width: 450px; height: 200px" alt="">
                                    <p>
                                        The Nile is a major north-flowing river in northeastern Africa,
                                        and is the longest river in Africa and the disputed longest river in the world.

                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="tile">
                                <h3 class="tile-title">Kasubi Tombs</h3>
                                <div class="tile-body tilk">
                                    <img src="images/kasubi.JPG" style="width: 450px; height: 200px" alt="">
                                    <p>
                                        The Kasubi Tombs in Kampala, Uganda, is the site of the burial grounds for four kabakas and
                                        other members of the Baganda royal family. As a result, the site remains an important spiritual and political
                                        site for the Ganda people, as well as an important example of traditional architecture

                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="tile">
                                <h3 class="tile-title">Ssesse inslands</h3>
                                <div class="tile-body tilk">
                                    <img src="images/ssese.JPG" style="width: 450px; height: 200px" alt="">
                                    <p>

                                        The Ssese Islands are an archipelago of eighty-four islands in the northwestern part
                                        of Lake Victoria in Uganda. The islands are coterminous with the Kalangala District in
                                        southern Central Uganda, which does not have any territory on mainland Uganda.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="tile">
                                <h3 class="tile-title">Bujagali Falls</h3>
                                <div class="tile-body tilk">
                                    <img src="images/bugali.JPG" style="width: 450px; height: 200px" alt="">
                                    <p>
                                        Bujagali Falls was a waterfall near Jinja in Uganda where the Nile River comes
                                        out of Lake Victoria, sometimes considered the source of the Nile.
                                        Starting November 2011, the falls were submerged by the new Bujagali Dam.

                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>














</main>
<!-- Essential javascripts for application to work-->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
<!-- The javascript plugin to display page loading on top-->
<script src="js/plugins/pace.min.js"></script>
<!-- Page specific javascripts-->
<script type="text/javascript" src="js/plugins/chart.js"></script>

</body>
</html>
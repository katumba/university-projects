<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <title>Login | Tourism Decision Support System</title>
  </head>
  <body>
    <section class="material-half-bg">
      <div class="cover"></div>
    </section>
    <section class="login-content">
      <div class="logo">
        <h1>Tourism Decision Support System</h1>
      </div>
      <div class="login-box">
        <form class="login-form" action="sign_up.php" method="POST">
          <h3 class="login-head">SIGN IN</h3>
          <div class="form-group">
            <label class="control-label">EMAIL</label>
            <input class="form-control" type="email" name="email" placeholder="Email" autofocus>
          </div>
          <div class="form-group">
            <label class="control-label">PASSWORD</label>
            <input class="form-control" type="password" name="password" placeholder="Password" required>
          </div> <br>
          <div class="form-group btn-container">
            <button class="btn btn-primary btn-block">SIGN IN</button>
          </div> <br>
          <div class="form-group">
            <div class="utility"> 
              <p class="semibold-text mb-2">Don't have an account? <a href="#" data-toggle="flip">Sign up</a></p>
            </div>
          </div>
        </form>

        <form class="forget-form" action="register.php" method="POST">
          <h3 class="login-head">SIGN UP</h3>
          <div class="form-group">
            <label class="control-label">FULL NAME</label>
            <input class="form-control" type="text" name="username" placeholder="Full name" required>
          </div>
          <div class="form-group">
            <label class="control-label">EMAIL</label>
            <input class="form-control" type="email" name="email" placeholder="Email" required>
          </div>
          <div class="form-group">
            <label class="control-label">PASSWORD</label>
            <input class="form-control" type="password" name="password" placeholder="Password" required>
          </div>
          <div class="form-group btn-container">
            <button type="submit" class="btn btn-primary btn-block" value="submit" >SIGN UP</button>
          </div>
        </form>
      </div>
    </section>



    <!-- Essential javascripts for application to work-->
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <!-- The javascript plugin to display page loading on top-->
    <script src="js/plugins/pace.min.js"></script>
    <script type="text/javascript">
      // Login Page Flipbox control
      $('.login-content [data-toggle="flip"]').click(function() {
      	$('.login-box').toggleClass('flipped');
      	return false;
      });
    </script>
  </body>
</html>

<?php

session_start();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="description" content="Decision Tourism Support System is a tourism system that will help tourists view places of Uganda that are worth to visit">

    <!-- Open Graph Meta-->
    <meta property="og:type" content="website">
    <meta property="og:site_name" content="Decision Tourism Support System">
    <meta property="og:title" content="Dashboard | Decision Tourism Support System">

    <title>Dashboard | Decision Tourism Support System </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Main CSS-->
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <!-- Font-icon css-->
    <link rel="stylesheet" href="css/font-awesome.min.css">
</head>
<body class="app sidebar-mini rtl">
<!-- Navbar-->
<header class="app-header"><a class="app-header__logo" href="#">TDS System</a>
    <!-- Sidebar toggle button--><a class="app-sidebar__toggle" href="#" data-toggle="sidebar" aria-label="Hide Sidebar"></a>
    <!-- Navbar Right Menu-->
    <ul class="app-nav">
        <li class="app-search">
            <input class="app-search__input" type="search" placeholder="Search">
            <button class="app-search__button"><i class="fa fa-search"></i></button>
        </li>
        <!-- User Menu-->
        <li class="dropdown"><a class="app-nav__item" href="#" data-toggle="dropdown" aria-label="Open Profile Menu"><i class="fa fa-user fa-lg"></i></a>
            <ul class="dropdown-menu settings-menu dropdown-menu-right">
                <li><a class="dropdown-item" href="#"><i class="fa fa-user fa-lg"></i> Profile</a></li>
                <li><a class="dropdown-item" href="index.php"><i class="fa fa-sign-out fa-lg"></i> Logout</a></li>
            </ul>
        </li>
    </ul>
</header>
<!-- Sidebar menu-->
<div class="app-sidebar__overlay" data-toggle="sidebar"></div>
<aside class="app-sidebar">
    <ul class="app-menu">
        <li><a class="app-menu__item active" href="dashboard.php"><i class="app-menu__icon fa fa-dashboard"></i><span class="app-menu__label">Dashboard</span></a></li>
        <li><a class="app-menu__item" href="tours.php"><i class="app-menu__icon fa fa-pie-chart"></i><span class="app-menu__label">Latest Tours</span></a></li>
        <li><a class="app-menu__item" href="game.php"><i class="app-menu__icon fa fa-pie-chart"></i><span class="app-menu__label">National Parks</span></a></li>
        <li><a class="app-menu__item" href="interesting.php"><i class="app-menu__icon fa fa-pie-chart"></i><span class="app-menu__label">Interesting Sites</span></a></li>
    </ul>
</aside>
<main class="app-content">
    <div class="app-title">
        <div>
            <h1>Booking</h1>
        </div>
        <ul class="app-breadcrumb breadcrumb">
            <li class="breadcrumb-item"><i class="fa fa-home fa-lg"></i></li>
            <li class="breadcrumb-item"><a href="#">Booking</a></li>
        </ul>
    </div>

    <div class="row">
        <div class="col-md-7 offset-2">
            <div class="tile">
                <h3 class="tile-title">Booking Form</h3>
                <hr>
                <div class="tile-body">
                    <form class="form-horizontal" action="book.php" method="POST">
                        <div class="form-group row">
                            <label class="control-label col-md-3">Name:</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="fullname" placeholder="Enter full name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Phone number:</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="phone" maxlength="10" minlength="10"   placeholder="Enter your phone number">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Email:</label>
                            <div class="col-md-8">
                                <input class="form-control" type="email" name="email" placeholder="Enter email address">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Gender</label>
                            <div class="col-md-9">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="gender">Male
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="radio" name="gender">Female
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Country:</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="country" placeholder="Enter your country where you come from">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Tourist site:</label>
                            <div class="col-md-8">
                                <input class="form-control" type="text" name="site" placeholder="Enter the tourist site name you want to tour">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Arrival Date:</label>
                            <div class="col-md-8">
                                <input class="form-control" type="date" name="arrivalDate">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Depart Date:</label>
                            <div class="col-md-8">
                                <input class="form-control" type="date" name="departDate" value="20-10-2018">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="control-label col-md-3">Comments</label>
                            <div class="col-md-8">
                                <textarea class="form-control" rows="3" name="comment" placeholder="Please describe your needs e.g. hotels, number of beds and among others"></textarea>
                            </div>
                        </div>
                        <div class="tile-footer">
                                    <button class="btn btn-success" type="submit" value="submit"><i class="fa fa-fw fa-lg fa-check-circle"></i>Submit</button>&nbsp;&nbsp;
                                    <button class="btn btn-primary" type="reset" value="reset"><i class="fa fa-fw fa-lg fa-check-circle"></i>Reset</button>&nbsp;&nbsp;
                                    &nbsp;<a class="btn btn-secondary" href="tours.php"><i class="fa fa-fw fa-lg fa-times-circle"></i>Cancel</a>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>





</main>
<!-- Essential javascripts for application to work-->
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/main.js"></script>
<!-- The javascript plugin to display page loading on top-->
<script src="js/plugins/pace.min.js"></script>
<!-- Page specific javascripts-->
<script type="text/javascript" src="js/plugins/chart.js"></script>

</body>
</html>